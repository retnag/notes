## systemd-resolved

#### Show assigned DNS servers:
  systemd-resolve --status |grep DNS\ Servers

#### Flush DNS cache:
  systemd-resolve --flush-caches
  Note: Youc an also restart the systemd-resolvd service, which also flushes the cache.

#### View statistics:
  systemd-resolve --statistics
  Note: This is one way to find out if the cache is clear

#### Disable listening on port 53 (this caused an issue with dnsmasq on pi-hole):
  Edit /etc/systemd/resolved.conf and add:
  DNSStubListener=no
