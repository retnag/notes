## Retropie

###### Force HDMI every boot
  Edit `/boot/config.txt`:
  Add these lines:
    `#Always force HDMI output and enable HDMI sound`
    `hdmi_force_hotplug=1`
    `hdmi_drive=2`

###### Steps for setting up a fresh Retropie
  * Install NTP
  * Set timezone
  * Enable HDMI fix (above)
  * Change megadrive branding to genesis
  * Copy over BIOS files
  * Copy over game lists and images
  * Configure MAME roms for free play, correct controls, etc
